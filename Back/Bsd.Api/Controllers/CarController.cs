﻿using System;
using Bsd.Api.Data.Interfaces;
using Bsd.Api.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Bsd.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly ICarRepository _carRepository;

        public CarController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        // GET: api/<CarController>
        [HttpGet]
        public async Task<IEnumerable<Car>> Get()
        {
            return _carRepository.GetAll();
        }

        // GET api/<CarController>/5
        [HttpGet("{id}")]
        public async Task<Car> Get(int id)
        {
            return _carRepository.GetById(id);
        }

        // POST api/<CarController>
        [HttpPost]
        public async Task<Car> Post([FromBody] Car value)
        {
            if (ModelState.IsValid)
            {
                return _carRepository.Insert(value);
            }
            else
            {
                throw new ArgumentException();
            }
           
        }

        // PUT api/<CarController>/5
        [HttpPut("{id}")]
        public async Task<Car> Put(int id, [FromBody] Car value)
        {
            if (ModelState.IsValid)
            {
                return _carRepository.Update(value, id);
            }
            else
            {
                throw new ArgumentException();
            }

           
        }

        // DELETE api/<CarController>/5
        [HttpDelete("{id}")]
        public async Task<Car> Delete(int id)
        {
            return _carRepository.Delete(id);
        }
    }
}
