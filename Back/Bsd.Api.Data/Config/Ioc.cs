﻿using System.Data.Entity;
using Bsd.Api.Data.Contexts;
using Bsd.Api.Data.Interfaces;
using Bsd.Api.Data.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bsd.Api.Data.Config
{
    public static class Ioc
    {
        public static IServiceCollection AddDatabaseServices(this IServiceCollection services, IConfiguration configuration)
        {
            var carContext = new CarContext(configuration["Data:ConnectionStrings:Cars"]);
            services.AddTransient<ICarRepository> (s => new CarRepository(carContext));
            return services;
        }
    }
}