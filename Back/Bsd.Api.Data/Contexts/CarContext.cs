﻿using System.Data.Entity;
using Bsd.Api.Data.Models;

namespace Bsd.Api.Data.Contexts
{
    public class CarContext : DbContext
    {
        public CarContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public DbSet<Car> Cars { get; set; }
    }
}