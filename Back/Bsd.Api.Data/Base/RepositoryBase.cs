﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Bsd.Api.Data.Base
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly DbContext _context = null;
        private readonly DbSet<T> table = null;

        protected RepositoryBase(DbContext _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }

        public IEnumerable<T> Where(Func<T, bool> lambda)
        {
            return table.Where( lambda);
        }
        public IEnumerable<T> GetAll()
        {
            return table.ToList();
        }
        public T GetById(int id)
        {
            return table.Find(id);
        }
        public T Insert(T obj)
        {
            var result = table.Add(obj);
            Save();
            return result;
        }
        public T Update(T obj, int id)
        {
            var origin = GetById(id);
            _context.Entry(origin).CurrentValues.SetValues(obj);
            Save();

            return obj;
        }
        public T Delete(int id)
        {
            T existing = table.Find(id);
            var result = table.Remove(existing);
            Save();
            return result;
        }
        private void Save()
        {
            _context.SaveChanges();
        }

    }
}