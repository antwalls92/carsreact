﻿using System;
using System.Collections.Generic;

namespace Bsd.Api.Data.Base
{
    public interface IRepositoryBase<T> 
    {
        IEnumerable<T> Where(Func<T, bool> lambda);

        IEnumerable<T> GetAll();

        T GetById(int id);

        T Insert(T obj);

        T Update(T obj, int id);

        T Delete(int id);


    }
}
