﻿using System.ComponentModel.DataAnnotations;

namespace Bsd.Api.Data.Models
{
    public class Car
    {
        public int Id { get; set; }

        [Required]
        public string InvoiceId { get; set; }

        [Required]
        [RegularExpression(@"[0-9]{4}-[A-Z]{3}")]
        public string CarFrame { get; set; }

        [Required]
        public string Model{ get; set; }

        [Required]
        [RegularExpression(@"[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}")]
        public string CarNumber{ get; set; }

        [Required]
        public string DeliverDate { get; set; }

        public void Update(Car copy)
        {
            Id = copy.Id;
            InvoiceId = copy.InvoiceId;
            CarFrame = copy.CarFrame;
            Model = copy.Model;
            CarNumber = copy.CarNumber;
            DeliverDate = copy.DeliverDate;
        }
    }
}