﻿using System.Data.Entity;
using Bsd.Api.Data.Base;
using Bsd.Api.Data.Interfaces;
using Bsd.Api.Data.Models;

namespace Bsd.Api.Data.Repository
{
    public class CarRepository : RepositoryBase<Car> , ICarRepository
    {
        public CarRepository(DbContext _context) : base(_context)
        {
        }
    }
}