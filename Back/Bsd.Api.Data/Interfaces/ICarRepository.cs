﻿using Bsd.Api.Data.Base;
using Bsd.Api.Data.Models;

namespace Bsd.Api.Data.Interfaces
{
    public interface ICarRepository : IRepositoryBase<Car>
    {

    }
}