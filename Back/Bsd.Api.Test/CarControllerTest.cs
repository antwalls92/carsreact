using System.Collections.Generic;
using Bsd.Api.Controllers;
using Bsd.Api.Data.Interfaces;
using Bsd.Api.Data.Models;
using NUnit.Framework;
using Moq;

namespace Bsd.Api.Test
{
    public class When_AllCars_Are_Requested
    {
        private CarController _subject;
        private Mock<ICarRepository> _carRepositoryMock;
        private IEnumerable<Car> _setupcars;


        [SetUp]
        public void Setup()
        {
            _carRepositoryMock = new Mock<ICarRepository>();
            _subject = new CarController(_carRepositoryMock.Object);

            _setupcars = new List<Car>() { new Car(), new Car() };
            _carRepositoryMock
                .Setup(x => x.GetAll())
                .Returns(_setupcars);
        }

        [Test]
        public void Repository_returns_them()
        {

            var cars = _subject.Get();

            _carRepositoryMock.Verify(x => x.GetAll(), Times.Once);
            Assert.AreEqual(cars, _setupcars);


        }
    }

    public class When_DeleteCar_is_Requested
    {
        private CarController _subject;
        private Mock<ICarRepository> _carRepositoryMock;
        private Car _car;


        [SetUp]
        public void Setup()
        {
            _carRepositoryMock = new Mock<ICarRepository>();
            _subject = new CarController(_carRepositoryMock.Object);

            _car =  new Car(){ Id = 10};
            _carRepositoryMock
                .Setup(x => x.Delete( 10 ))
                .Returns(_car);
        }

        [Test]
        public void Repository_returns_them()
        {

            var car = _subject.Delete(10);

            _carRepositoryMock.Verify(x => x.Delete(10), Times.Once);
            Assert.AreEqual(car, _car);


        }
    }



    public class When_UpdateCar_is_Requested
    {
        private CarController _subject;
        private Mock<ICarRepository> _carRepositoryMock;
        private Car _car;


        [SetUp]
        public void Setup()
        {
            _carRepositoryMock = new Mock<ICarRepository>();
            _subject = new CarController(_carRepositoryMock.Object);

            _car = new Car() { Id = 10, CarFrame = "carframe"};
            _carRepositoryMock
                .Setup(x => x.Update(_car, _car.Id))
                .Returns(_car);
        }

        [Test]
        public void Repository_returns_them()
        {

            var car = _subject.Put(_car.Id, _car);

            _carRepositoryMock.Verify(x => x.Update(car, car.Id), Times.Once);
            Assert.AreEqual(car, _car);


        }
    }


    public class When_CreateCar_is_Requested
    {
        private CarController _subject;
        private Mock<ICarRepository> _carRepositoryMock;
        private Car _car;


        [SetUp]
        public void Setup()
        {
            _carRepositoryMock = new Mock<ICarRepository>();
            _subject = new CarController(_carRepositoryMock.Object);

            _car = new Car() { Id = 10, CarFrame = "carframe" };
            _carRepositoryMock
                .Setup(x => x.Insert(_car))
                .Returns(_car);
        }

        [Test]
        public void Repository_returns_them()
        {

            var car = _subject.Post(_car);

            _carRepositoryMock.Verify(x => x.Insert(car), Times.Once);
            Assert.AreEqual(car, _car);


        }
    }
}