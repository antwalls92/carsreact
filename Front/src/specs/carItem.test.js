import React from 'react';
import CarItem  from '../components/carItemComponent';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
 

describe('Car item component', () => {

    let props;

    beforeEach(() => {
        props = {
            car: {"id":1,"invoiceId":"12341","carFrame":"1111","model":"Tesla Model 3","carNumber":"1111111","deliverDate":"2020-08-19"},
            updateCarRequest : jest.fn(),
            createCarRequest : jest.fn(),
            readCarRequest: jest.fn(),
            deleteCarRequest : jest.fn(),
          }
      });

    it('renders without crashing', () => {  
        const tree = renderer.create(<CarItem {...props} />);
        expect(tree.toJSON()).toMatchSnapshot();
    });
    it('show the expected elements ', () => {  
        const wrapper = shallow(<CarItem {...props} />);
        
        expect(wrapper.find('td').at(0).text()).toEqual("12341")
        expect(wrapper.find('td').at(1).text()).toEqual("Tesla Model 3") 
        expect(wrapper.find('td').at(2).text()).toEqual("1111111") 
        expect(wrapper.find('td').at(3).text()).toEqual("1111") 
        expect(wrapper.find('td').at(4).text()).toEqual("2020-08-19") 
        expect(wrapper.find('td').at(5).find('button[name="update"]').length).toEqual(1)
        expect(wrapper.find('td').at(6).find('button[name="delete"]').length).toEqual(1)
    });

    it('delete the element when button clicked ', () => {  
        const wrapper = shallow(<CarItem {...props} />);
        
        var button = wrapper.find('td').at(5).find('button[name="update"]')
        button.simulate('click')

        expect(props.updateCarRequest).toHaveBeenCalled();
    });

    it('edit the element when button clicked', () => {  
        const wrapper = shallow(<CarItem {...props} />);

        var button = wrapper.find('td').at(6).find('button[name="delete"]')
        button.simulate('click')

        expect(props.deleteCarRequest).toHaveBeenCalled();
    });


});
