import React from 'react';
import CarEdit  from '../components/carEditCompopnent';
import {ExportFunctions}  from '../components/carEditCompopnent';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
 

describe('Car Edit Modal component', () => {

    let props;

    beforeEach(() => {
        props = {
            cars: [],
            editing: false,
        
          }
      });

    it('renders without crashing', () => {  
        const tree = renderer.create(<CarEdit/>);
        expect(tree.toJSON()).toMatchSnapshot();
    });
    it('show when editing and not open', () => {  
        const wrapper = shallow(<CarEdit editing={false} show ={false}  />);
        const spy = jest.spyOn(ExportFunctions, 'handleShow');
    
        props.editing = {};
        wrapper.setProps(props)
       
        expect(spy).toHaveBeenCalled();
    });

    it('hide when not editing and open', () => {  
        const wrapper = shallow(<CarEdit editing={{}} show ={true}  />);
        const spy = jest.spyOn(ExportFunctions,'handleClose');
    
        props.editing = false;
        wrapper.setProps(props)

        expect(spy).toHaveBeenCalled();
    });
    
});
