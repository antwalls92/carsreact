import React from 'react';
import CarList  from '../components/carListComponent';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
 

describe('Car List component', () => {

    let props;

    beforeEach(() => {
        props = {
            cars: [{"id":1,"invoiceId":"12341","carFrame":"1111","model":"Tesla Model 3","carNumber":"1111111","deliverDate":"2020-08-19"},{"id":2,"invoiceId":"321451","carFrame":"1224453435435f","model":"Mazda 3","carNumber":"3016 LCB","deliverDate":"2020-08-13"},{"id":3,"invoiceId":"5555","carFrame":"55555","model":"Nissan GTR","carNumber":"66666","deliverDate":"2020-08-28"},{"id":19,"invoiceId":"12346","carFrame":"1214151661","model":"Honda civic","carNumber":"500 gxx","deliverDate":"2020-08-20"}],
            editing: false,
            actions:{
                createCarRequest : jest.fn(),
                readCarsRequest : jest.fn(),
                updateCarRequest : jest.fn(),
                deleteCarRequest : jest.fn(),
            }
          }
      });

    it('renders without crashing', () => {  
        const tree = renderer.create(<CarList {...props} />);
        expect(tree.toJSON()).toMatchSnapshot();
    });
    it('show the expected elements ', () => {  
        const wrapper = shallow(<CarList {...props} />);
        expect(wrapper.find('table').length).toEqual(1)
        expect(wrapper.find('CarItem').length).toEqual(4)
    });


});
