import React from 'react';
import CarForm  from '../components/carFormComponent';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
 

describe('Car Form component', () => {

    let props;

    beforeEach(() => {
        props = {
            cars: [],
            editing: {"id":1,"invoiceId":"12341","carFrame":"1111","model":"Mazda 3","carNumber":"1111111","deliverDate":"2020-08-19"},
            actions:{
                submitCar : jest.fn(),
                cancelEditCar : jest.fn()
            }
          }
      });

    it('renders without crashing', () => {  
        const tree = renderer.create(<CarForm {...props} />);
        expect(tree.toJSON()).toMatchSnapshot();
    });
    it('fill the state with the expected values', () => {  
        const wrapper = shallow(<CarForm {...props} />);

        expect(wrapper.state().id).toEqual(1);
        expect(wrapper.state().invoiceId).toEqual("12341");
        expect(wrapper.state().carFrame).toEqual("1111");
        expect(wrapper.state().model).toEqual("Mazda 3");
        expect(wrapper.state().carNumber).toEqual("1111111");
        expect(wrapper.state().deliverDate).toEqual("2020-08-19");
     
    });

    it('fill the inputs with the values and updates state accordingly', () => {  
        const wrapper = shallow(<CarForm {...props} />);
      
        wrapper.find('input[name="id"]').simulate('change', { target: { name: 'id',  value: 1 } })
        wrapper.find('FormItemComponent').find('[name="invoiceId"]').shallow().find('input[name="invoiceId"]').simulate('change', { preventDefault: () => {}, target: { name: 'invoiceId', value: "Invoice Id" } })
        wrapper.find('FormItemComponent').find('[name="carFrame"]').shallow().find('input[name="carFrame"]').simulate('change', { preventDefault: () => {}, target: { name: 'carFrame', value: "0500-GXX" } })
        wrapper.find('FormItemComponent').find('[name="model"]').shallow().find('input[name="model"]').simulate('change', { preventDefault: () => {}, target: { name: 'model', value: "Car Model" } })
        wrapper.find('FormItemComponent').find('[name="carNumber"]').shallow().find('input[name="carNumber"]').simulate('change', { preventDefault: () => {}, target: { name: 'carNumber', value: "X1234678X" } })
        wrapper.find('FormItemComponent').find('[name="deliverDate"]').shallow().find('input[name="deliverDate"]').simulate('change', { preventDefault: () => {}, target: { name: 'deliverDate', value: "29/07/2018" } })

  
        expect(wrapper.state().id).toEqual(1);
        expect(wrapper.state().invoiceId).toEqual("Invoice Id");
        expect(wrapper.state().carFrame).toEqual("0500-GXX");
        expect(wrapper.state().model).toEqual("Car Model");
        expect(wrapper.state().carNumber).toEqual("X1234678X");
        expect(wrapper.state().deliverDate).toEqual("29/07/2018");
     
    });


    it('On submit it will dispatch the proper action', () => {  
        const wrapper = shallow(<CarForm {...props} />);
        const spy = jest.spyOn(wrapper.instance(), 'handleSubmit');

        wrapper.find('form').first().simulate('submit',{ preventDefault: () => {}});

        expect(props.actions.submitCar).toHaveBeenCalled();
    });


    it('On cancel it will dispatch the proper action', () => {  
        const wrapper = shallow(<CarForm {...props} />);
        const spy = jest.spyOn(wrapper.instance(), 'handleCancel');

        wrapper.find('input[name="cancel"]').first().simulate('click');

        expect(props.actions.cancelEditCar).toHaveBeenCalled();
    });


    it('On submit, if the car number has not valid format, it shows an error', () => {  
        const wrapper = shallow(<CarForm {...props} />);

       
    });


    
});
