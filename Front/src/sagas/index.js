import { fork, all } from 'redux-saga/effects'
import CarSaga from './carSaga'

export default function * root () {
  yield all([
    fork(CarSaga.watchCreateCar),
    fork(CarSaga.watchReadCars),
    fork(CarSaga.watchUpdateCar),
    fork(CarSaga.watchDeleteCar)
  ])
}