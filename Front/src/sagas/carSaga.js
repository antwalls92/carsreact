import { call, put, takeEvery, takeLatest, take } from 'redux-saga/effects'
import { CarActions } from '../actions/carActions'
import CarApi from '../services/carsApi'

//   type: 'CREATE_CAR',
function * CreateCar (car) {
    try {
      yield put({ type: CarActions.CREATE_CAR_EDITING });
      var action = yield take(CarActions.CAR_SUBMIT )

      yield call(CarApi.createCar,action.car);          

      yield put({ type: CarActions.CREATE_CAR_SUCCESS, car} );

      yield put({ type: CarActions.READ_CARS_REQUEST });

    } catch (error) {
      yield put({ type: CarActions.CREATE_CAR_ERROR, error });
    }
}

function * watchCreateCar () {
    yield takeEvery(CarActions.CREATE_CAR_REQUEST, CreateCar)
}

//   type: 'READ_CARS'
function* ReadCars () {
    try {

      const cars = yield call(CarApi.getCars)
      yield put({ type: CarActions.READ_CARS_SUCCESS, cars })

    } catch (error) {
      yield put({ type: CarActions.READ_CARS_ERROR, error });
    }
}
  
function * watchReadCars () {
    yield takeEvery(CarActions.READ_CARS_REQUEST, ReadCars)
}

//   type: 'UPDATE_CARS'
function * UpdateCar (action) {
    try {
      var car = action.car;
      yield put({ type: CarActions.UPDATE_CAR_EDITING, car });
      var actionSubmit = yield take(CarActions.CAR_SUBMIT)
      car = actionSubmit.car;

      yield call(CarApi.updateCar, car);

      yield put({ type: CarActions.UPDATE_CAR_SUCCESS, car })

      yield put({ type: CarActions.READ_CARS_REQUEST });

    } catch (error) {
      yield put({ type: CarActions.UPDATE_CAR_ERROR, error });
    }
}
function * watchUpdateCar () {
    yield takeEvery(CarActions.UPDATE_CAR_REQUEST, UpdateCar)
}


//   type: 'DELETE_CAR',
function * DeleteCar (action) {
    try {

      const returnedcar = yield call(CarApi.deleteCar, action.id)
      yield put({ type: CarActions.DELETE_CAR_SUCCESS, returnedcar })

      yield put({ type: CarActions.READ_CARS_REQUEST });

    } catch (error) {
      yield put({ type: CarActions.DELETE_CAR_ERROR, error });
    }
}
function * watchDeleteCar () {
    yield takeEvery(CarActions.DELETE_CAR_REQUEST, DeleteCar)
}


const CarSaga = {
  watchCreateCar, 
  watchReadCars, 
  watchUpdateCar, 
  watchDeleteCar
}

export default CarSaga;