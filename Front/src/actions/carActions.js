export const CarActions = {
 'CREATE_CAR_REQUEST' : 'CREATE_CAR_REQUEST',
 'CREATE_CAR_ERROR':'CREATE_CAR_ERROR',
 'CREATE_CAR_EDITING':'CREATE_CAR_EDITING',
 'CREATE_CAR_SUCCESS':'CREATE_CAR_SUCCESS',

 'READ_CARS_REQUEST' : 'READ_CARS_REQUEST', 
 'READ_CARS_ERROR' : 'READ_CARS_ERROR', 
 'READ_CARS_SUCCESS' : 'READ_CARS_SUCCESS',  

 'UPDATE_CAR_REQUEST' : 'UPDATE_CAR_REQUEST',
 'UPDATE_CAR_ERROR' : 'UPDATE_CAR_ERROR',
 'UPDATE_CAR_EDITING':'UPDATE_CAR_EDITING',
 'UPDATE_CAR_SUCCESS' : 'UPDATE_CAR_SUCCESS', 

 'CANCEL_EDIT':'CANCEL_EDIT',
 'CAR_SUBMIT':'CAR_SUBMIT',
 
 'DELETE_CAR_REQUEST' : 'DELETE_CAR_REQUEST',
 'DELETE_CAR_ERROR' : 'DELETE_CAR_ERROR',
 'DELETE_CAR_SUCCESS' : 'DELETE_CAR_SUCCESS'
 }



 function makeActionCreator(type, ...argNames) {
  return function (...args) {
    const action = { type }
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index]
    })
    return action
  }
}

const createCarRequest = makeActionCreator(CarActions.CREATE_CAR_REQUEST)
const createCarEditing = makeActionCreator(CarActions.CREATE_CAR_EDITING)
const createCarError = makeActionCreator(CarActions.CREATE_CAR_ERROR, 'error')
const createCarSuccess = makeActionCreator(CarActions.CREATE_CAR_SUCCESS,'car')

const readCarsRequest = makeActionCreator(CarActions.READ_CARS_REQUEST)
const readCarsSuccess = makeActionCreator(CarActions.READ_CARS_SUCCESS, 'cars')
const readCarsError = makeActionCreator(CarActions.READ_CARS_ERROR, 'error')

const updateCarRequest = makeActionCreator(CarActions.UPDATE_CAR_REQUEST,'car')
const updateCarEditing = makeActionCreator(CarActions.UPDATE_CAR_EDITING)
const updateCarError = makeActionCreator(CarActions.UPDATE_CAR_ERROR,'error')
const updateCarSuccess = makeActionCreator(CarActions.UPDATE_CAR_SUCCESS)

const deleteCarRequest = makeActionCreator(CarActions.DELETE_CAR_REQUEST,'id')
const deleteCarError = makeActionCreator(CarActions.DELETE_CAR_ERROR,'error')
const deleteCarSuscess = makeActionCreator(CarActions.DELETE_CAR_SUCCESS,'car')

const submitCar = makeActionCreator(CarActions.CAR_SUBMIT,'car')

const cancelEditCar = makeActionCreator(CarActions.CANCEL_EDIT)


const CarActionCreators = {
  createCarRequest,
  createCarEditing,
  createCarError,
  createCarSuccess,
  readCarsRequest,
  readCarsSuccess,
  readCarsError,
  updateCarRequest,
  updateCarEditing,
  updateCarError,
  updateCarSuccess,
  deleteCarRequest,
  deleteCarError,
  deleteCarSuscess,
  submitCar,
  cancelEditCar
}

export default CarActionCreators;