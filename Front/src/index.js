import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import configureStore from './store'
import CarListPage from './containers/carListContainer';
import 'bootstrap/dist/css/bootstrap.css';


const store = configureStore()

ReactDOM.render(

  <>
    <nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand">Cars Invoices</a>
    </nav>
    <div class="container">
      <Provider store={store}>
        <CarListPage />
      </Provider>
    </div>
  </>
  ,
  
  document.getElementById('root')
);
