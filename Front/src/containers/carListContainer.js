import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import CarList from '../components/carListComponent'
import CarActionCreators from '../actions/carActions'

export function mapStateToProps (state) {
  return  state.cars
}

export function mapDispatchToProps (dispatch) {
  
  return {
    actions: bindActionCreators({
      createCarRequest: CarActionCreators.createCarRequest, 
      readCarsRequest: CarActionCreators.readCarsRequest, 
      updateCarRequest: CarActionCreators.updateCarRequest, 
      deleteCarRequest: CarActionCreators.deleteCarRequest, 
      cancelEditCar: CarActionCreators.cancelEditCar, 
      submitCar: CarActionCreators.submitCar,
    },dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarList)