import React from 'react';

export class FormItemComponent extends React.Component {
 
     
    constructor(props){
      super(props);
      this.state = {[this.props.name] : this.props.initialValue};
      this.validate(this.props.initialValue);

    }

  inputValue(){
    return this.state[this.props.name]
  }

  showError(message)
  {
    this.setState({error: message})
  }
  clearError()
  {
    this.setState({error: undefined})
  }

  changeFormVal(nam, val){
    this.props.changeState(nam, val)
  }

  validateMaxLength(val){
    if(typeof(this.props.maxlength) !== 'undefined')
    {
       if(val.lenght > this.props.maxlength){
        this.showError("The field can be only " + this.props.maxlength + " characters long")
        return false;
       }else{
         return true;
       }
    }else{
      return true;
    }
  }

  validateRegex(val){
    if( this.props.regex instanceof RegExp)
    {
       if(!(this.props.regex.test(val))){
        this.showError("The field has not correct format")
        return false;
       }else{
         return true;
       }
    }else{
      return true;
    }
  }

  validateRequired(val){
    if(typeof(this.props.required) !== 'undefined')
    {
       if(val.lenght <= 0 ){
        this.showError("The field cannot be empty")
        return false;
       }else{
         return true;
       }
    }else{
      return true;
    }
  }

  validateType(val){
    switch (this.props.type) {
      case "number":{
        return !isNaN(val)
        break;
      }
      case "date":{
        return true
        break;
      }
      default:{
        return true;
        break;
      }
    }
  }

  validate(val){

    var validatee = this.validateMaxLength(val) &&
    this.validateRegex(val) &&
    this.validateType(val) &&
    this.validateRequired(val);

    if(validatee){
      this.clearError();
      this.changeFormVal("validation."+this.props.name, true)
    }else{
      this.changeFormVal("validation."+this.props.name, false)
    }
    return validatee;
    
  }

  handleInputChange = (event) => {
    event.preventDefault();
   
    let nam = event.target.name;
    let val = event.target.value || "";
    this.setState({[nam]: val});

    if(this.validate(val)){
      this.changeFormVal(nam, val)
    }
   
  }

  errorclass= () =>{
    if((this.inputValue()) !== "")
      return this.state.error !== undefined ? "is-invalid" : "is-valid";
    else
      return ""
  } 

  render() {
    const car = this.props.car; 
   
    return (
        <div class="form-group">
        <label>{this.props.label}</label>    
        <input 
            class={"col-sm-12 form-control " + this.errorclass() }
            type={this.props.type} 
            name={this.props.name} 
            value={this.inputValue()} 
            onChange={this.handleInputChange} />
        {this.state.error !== null ?
            <span class="text-danger">{this.state.error}</span>
        : null }
      </div>
    );
  }
}

export default FormItemComponent