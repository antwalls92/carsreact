import React from 'react';
import FormItem from "./formItemComponent"
export class CarForm extends React.Component {
  
  constructor(props){
    super(props)
    const editing = props.editing;

    this.state = { 
      id: editing.id || 0,
      invoiceId: editing.invoiceId || '',
      carFrame: editing.carFrame ||'',
      model: editing.model || '',
      carNumber: editing.carNumber || '',
      deliverDate: editing.deliverDate || '' 
    }
    
    this.submitCar = this.props.actions.submitCar;
    this.cancelEditCar = this.props.actions.cancelEditCar;
    this.changeState = this.changeState.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this); 
  }

  handleSubmit(event) {
    event.preventDefault();
    if((this.isValid())){
      this.submitCar(this.state);
    }
  }
  handleCancel(){
    this.cancelEditCar();
  }

  changeState = (key, value) => {
    this.setState({[key]: value});
  }

  isValid(){
    return Object.keys(this.state).filter(function(k) {
      return k.startsWith('validation.')})
              .every(v => this.state[v] === true);
  }

  render(){
      return (
      <form class="container" onSubmit={this.handleSubmit}>
        <input type="hidden" name="id" value={this.state.id} onChange={this.changeState} />
        
        <FormItem type="text" 
        name="invoiceId" 
        label="Invoice:" 
        required
        changeState={this.changeState}
        initialValue={this.state.invoiceId}/>
        
        <FormItem type="text" 
        name="model" 
        label="Model:" 
        required
        changeState={this.changeState}
        initialValue={this.state.model}/>

       <FormItem type="text" 
        name="carFrame" 
        label="Car Frame:" 
        required
        regex={/[0-9]{4}-[A-Z]{3}/}
        changeState={this.changeState}
        initialValue={this.state.carFrame}/>

        <FormItem type="text" 
        name="carNumber" 
        label="Car Number:" 
        required
        regex={/[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}/}
        changeState={this.changeState}
        initialValue={this.state.carNumber}/>

        <FormItem type="date" 
        name="deliverDate" 
        label="Deliver Date:" 
        required
        changeState={this.changeState}
        initialValue={this.state.deliverDate}/>

        <br/>

        <input type="button" name="cancel" onClick={this.handleCancel} class="btn btn-secondary col-md-3" value="Cancel" />
        <input type="submit" name="submit" class="btn btn-primary col-md-3" value="Save" />

      </form>)
  }
}

export default CarForm