import React from 'react';
import Modal from 'react-bootstrap/Modal';
import CarForm from '../components/carFormComponent';
import Container from 'react-bootstrap/Container';  


function CarEdit(props){
 
  const [show, setShow] = React.useState(false);
  const editing = props.editing;  
 
  if(!editing && show){
    ExportFunctions.handleClose(setShow);
  }
  if(editing && !show){
    ExportFunctions.handleShow(setShow);
  }

  return (
    
    <Modal show={show}>
      <Modal.Header>
        <Modal.Title>Edit Invoice</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>  
          <CarForm {...props}/>
        </Container>  
      </Modal.Body>
    </Modal>    
  );
    
}
function handleClose(setShow) {setShow(false);} 
function handleShow (setShow) {setShow(true);} 

export const ExportFunctions = {
  handleClose,
  handleShow
};
  
  export default CarEdit