import React from 'react';

export class CarItem extends React.Component {
 
  updateElement(car){
    this.props.updateCarRequest(car)
  }

  deleteElement(car){
    this.props.deleteCarRequest(car.id)
  }

  render() {
    const car = this.props.car; 
    return (
      <tr>
        <td>{car.invoiceId}</td>
        <td>{car.model}</td>
        <td>{car.carNumber}</td>
        <td>{car.carFrame}</td>
        <td>{car.deliverDate}</td>
        <td><button name="update" onClick={() => this.updateElement(car)} class="btn btn-secondary">Edit Element</button> </td>
        <td><button name="delete" onClick={() => this.deleteElement(car)} class="btn btn-danger">Delete Element</button> </td>
      </tr>
    );
  }
}

export default CarItem