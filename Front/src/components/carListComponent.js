import React from 'react';
import CarItem from './carItemComponent'
import CarEdit from '../components/carEditCompopnent'

export class CarList extends React.Component {
  componentDidMount(){
    this.props.actions.readCarsRequest()
  }

  addElement(){
    this.props.actions.createCarRequest();
  }

  render() {

    const items = this.props.cars.map( 
      (item) => 
          <CarItem 
            createCarRequest={this.props.actions.createCarRequest} 
            readCarsRequest={this.props.actions.readCarsRequest} 
            updateCarRequest={this.props.actions.updateCarRequest} 
            deleteCarRequest={this.props.actions.deleteCarRequest} 
            car={item}  
          />
    )

    return (
      <>
        <CarEdit {...this.props}/>
        <td> <button onClick={() => this.addElement()} class="btn btn-primary">Add Element</button> </td>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Invoice Number</th>
              <th scope="col">Model</th>
              <th scope="col">Plate Number</th>
              <th scope="col">Car Number</th>
              <th scope="col">Delivery Date</th>
              <th scope="col"></th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {items}
          </tbody>
        </table>
      </>
      
      );
  }
}


export default CarList