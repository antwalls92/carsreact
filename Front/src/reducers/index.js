import {combineReducers} from 'redux'
import cars from "./carsReducer"

const reducers = {
  cars
}

const appReducer = combineReducers(reducers)

const rootReducer = (state, action) => {
  return appReducer(state, action)
}

export default rootReducer