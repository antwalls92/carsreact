import { CarActions } from "../actions/carActions";

export function initialState () {
    return {cars: [], error: null, success: null, editing:false}
  }
  
  export default function (state = initialState(), action) {
    if(action){
        switch(action.type){

          //CREATE
          case CarActions.CREATE_CAR_EDITING : {
              return Object.assign({}, state, {
                  editing: true
                })
          }
          case CarActions.CREATE_CAR_SUCCESS : {
              return Object.assign({}, state, {
                  success: "Car "+ action.car.model + " created correctly"
                })
          }
          case CarActions.CREATE_CAR_ERROR : {
            return Object.assign({}, state, {
                editing:false,
                error: "Error creating Car "
              })
          }


          //SUBMIT
          case CarActions.CANCEL_EDIT : {
            return Object.assign({}, state, {
                editing: false
              })
          }
          case CarActions.CAR_SUBMIT : {
            return Object.assign({}, state, {
                editing: false
              })
          }


          //DELETE
          case CarActions.DELETE_CAR_SUCCESS : {
              return Object.assign({}, state, {
                  success: "Car deleted correctly"
                })
          }


          //READ
          case CarActions.READ_CARS_REQUEST : {
            return Object.assign({}, state)
          }
          
          case CarActions.READ_CARS_SUCCESS : {
              return Object.assign({}, state, {
                  cars: action.cars,
                  success: "Results retrieved"
                })
          }


          //UPDATE
          case CarActions.UPDATE_CAR_EDITING : {
            return Object.assign({}, state, {
                editing: action.car
              })
          }
          case CarActions.UPDATE_CAR_SUCCESS : {
              return Object.assign({}, state, {
                  success: "Car "+ action.car.model+ " updated correctly"
                })
          }
          case CarActions.UPDATE_CAR_ERROR : {
            return Object.assign({}, state, {
                editing:false,
                error: "Error update Car "
              })
          }
          
          default:
              return state;
      }
    }else{
      return state;
    }
    
  }