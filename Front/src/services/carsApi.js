import { Config } from "../config.dev"

 function getCars(){

  var apiUrl = Config.apiUrl;
  return new Promise((resolve, reject) => {
    fetch(apiUrl,{
      mode: 'cors'
    })
      .then(response => {
        if (response.ok) {
          response.json().then(resolve).catch(reject)
        } else {
          response.json().then(errJson => {
            console.log(errJson)
          }).catch(reject)
        }
      }).catch( response => {
        alert('Error')
      }) 
    })
}

 function createCar(car){

  var apiUrl = Config.apiUrl;
  return new Promise((resolve, reject) => {
    fetch(apiUrl,{
      method: 'post',
      mode: 'cors',
      headers:{
        "content-type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(car)
    }).then(response => {
        if (response.ok) {
          response.json().then(resolve).catch(reject)
        } else {
          response.json().then(errJson => {
            console.log(errJson)
          }).catch(reject)
        }
      }).catch( response => {
        alert('Error')
      }) 
    })
}
  
 function getCar(id){}
 function deleteCar(id){
  var apiUrl = Config.apiUrl;
  return new Promise((resolve, reject) => {
    fetch(apiUrl + '/' + id, {
      method: 'delete',
      mode: 'cors',
      headers:{
        "content-type": "application/json; charset=utf-8"
      },
    }).then(response => {
        if (response.ok) {
          response.json().then(resolve).catch(reject)
        } else {
          response.json().then(errJson => {
            console.log(errJson)
          }).catch(reject)
        }
      }).catch( response => {
        alert('Error')
      }) 
    })
}
 function updateCar(car){
  var apiUrl = Config.apiUrl;
  return new Promise((resolve, reject) => {
    fetch(apiUrl + '/' + car.id,{
      method: 'put',
      mode: 'cors',
      headers:{
        "content-type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(car)
    }).then(response => {
        if (response.ok) {
          response.json().then(resolve).catch(reject)
        } else {
          response.json().then(errJson => {
            console.log(errJson)
          }).catch(reject)
        }
      }).catch( response => {
        alert('Error')
      }) 
    })
}

const CarApi = {
  getCars, 
  createCar, 
  deleteCar, 
  updateCar
}

export default CarApi;